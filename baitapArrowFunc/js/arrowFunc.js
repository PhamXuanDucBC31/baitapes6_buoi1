const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let renderColor = () => {
  let contentHTML = "";
  for (let index = 0; index < colorList.length; index++) {
    var color = colorList[index];
    let contentBtn = `<div class="color-button ${color}" id="${index}" onclick="change('${color}',id)"></div>`;
    if (index == 0) {
      contentBtn = `<div class="color-button ${color} active" id="${index}" onclick="change('${color}',id)"></div>`;
    }
    contentHTML = contentHTML + contentBtn;
  }
  document.getElementById("colorContainer").innerHTML = contentHTML;
};

renderColor();

let change = (value, id) => {
  var current = document.getElementById("house").classList;
  if (current.length == 1) {
    document.getElementById("house").classList.add(value);
  } else {
    document.getElementById("house").classList.remove(current[1]);
    document.getElementById("house").classList.add(value);
  }

  for (let index = 0; index < colorList.length; index++) {
    document.getElementById(index).classList.remove("active");
  }

  document.getElementById(id).classList.add("active");
};
