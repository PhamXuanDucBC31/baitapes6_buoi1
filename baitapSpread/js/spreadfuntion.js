let h2_headingText = document.querySelector(".heading").innerHTML;
let spreadText = [...h2_headingText];

let renderText = (b) => {
  document.querySelector(".heading").innerHTML = "";
  let contentHTML = "";
  for (let index = 0; index < b.length; index++) {
    var contentSpan = `<span>${b[index]}</span>`;
    contentHTML = contentHTML + contentSpan;
    document.querySelector(".heading").innerHTML = contentHTML;
  }
};

renderText(spreadText);
